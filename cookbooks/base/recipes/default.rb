user 'ubuntu' do
  home '/home/ubuntu'
  supports manage_home: true
end

%w(build-essential git tree locate ack-grep python-pip unzip dnsmasq).each do |app|
  package app
end

directory '/home/ubuntu/.ssh' do
  owner 'ubuntu'
  group 'ubuntu'
  mode '0700'
end

search(:ssh_keys, 'id:*')
ssh_keys = search(:ssh_keys, 'id:*').map {|k| k['key']}

template '/home/ubuntu/.ssh/authorized_keys' do
  source 'authorized_keys.erb'
  variables ssh_keys: ssh_keys
  owner 'ubuntu'
  group 'ubuntu'
  mode '0600'
end

cookbook_file "/etc/dnsmasq.d/10-consul" do
  source "dnsmasq-consul"
end
