require 'serverspec'

set :backend, :exec

%w(build-essential git tree locate ack-grep python-pip unzip dnsmasq).each do |app|
  describe package(app) do
    it { is_expected.to be_installed }
  end
end

describe 'ubuntu user' do
  describe file('/home/ubuntu') do
    it { is_expected.to be_directory }
  end

  describe file('/home/ubuntu/.ssh') do
    it { is_expected.to be_directory }
    it { is_expected.to be_owned_by 'ubuntu' }
    it { is_expected.to be_grouped_into 'ubuntu' }
    it { is_expected.to be_mode '700' }
  end

  describe file('/home/ubuntu/.ssh/authorized_keys') do
    it { is_expected.to be_file }
    it { is_expected.to be_owned_by 'ubuntu' }
    it { is_expected.to be_grouped_into 'ubuntu' }
    it { is_expected.to be_mode '600' }

    its(:content) { is_expected.to eq("ssh-key\n") }
  end
end

describe 'dnsmasq' do
  describe file('/etc/dnsmasq.d/10-consul') do
    it { is_expected.to be_file }

    its(:content) { is_expected.to eq("server=/consul/127.0.0.1#8600\n") }
  end

  describe port(53) do
    it { is_expected.to be_listening.on('0.0.0.0').with('udp') }
    it { is_expected.to be_listening.on('0.0.0.0').with('tcp') }
  end

  describe process('dnsmasq') do
    it { is_expected.to be_running }
    its(:user) { is_expected.to eq 'dnsmasq' }
  end
end

describe 'SSH' do
  describe file('/etc/ssh/sshd_config') do
    it { is_expected.to be_file }
    its(:content) { is_expected.to match /^PermitRootLogin no$/ }
    its(:content) { is_expected.to match /^PasswordAuthentication no$/ }
  end

  describe port(22) do
    it { is_expected.to be_listening.with('tcp') }
  end
end
